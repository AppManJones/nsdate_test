//
//  RootViewController.m
//  tet
//
//  Created by Joe Rouleau on 1/8/15.
//  Copyright (c) 2015  Joe Rouleau. All rights reserved.
//

#import "RootViewController.h"
#import "NSDateFormatter+Predefined.h"

static NSInteger const secondsPerDay = 60 * 60 * 24;
static CGFloat const firstDateInterval = 365;
static CGFloat const secondDateInterval = 731;

@interface RootViewController ()

@property (nonatomic, strong) NSDate *firstDate;
@property (nonatomic, strong) NSDate *secondDate;
@property (nonatomic, strong) NSDateFormatter *dateFormatter;

@property (weak, nonatomic) IBOutlet UILabel *firstDateLabel;
@property (weak, nonatomic) IBOutlet UILabel *secondDateLabel;
@property (weak, nonatomic) IBOutlet UILabel *outputLabel;

- (IBAction)calculateWithNSCalendar:(id)sender;

@end

@implementation RootViewController

#pragma mark - View Controller Life Cycle

- (void)viewDidLoad;
{
  [super viewDidLoad];
  
  self.firstDateLabel.text = [[NSDateFormatter defaultDateFormatter] stringFromDate:self.firstDate];
  self.secondDateLabel.text = [[NSDateFormatter defaultDateFormatter] stringFromDate:self.secondDate];
}

- (void)didReceiveMemoryWarning;
{
  [super didReceiveMemoryWarning];
}

#pragma mark - Accessors

- (NSDate *)firstDate;
{
  if( !_firstDate ) {
	NSTimeInterval interval = (firstDateInterval * secondsPerDay) + 19800;
	_firstDate = [NSDate dateWithTimeIntervalSince1970:interval];
  }
  return _firstDate;
}

- (NSDate *)secondDate;
{
  if( !_secondDate ) {
	NSTimeInterval interval = (secondDateInterval * secondsPerDay) + 41040;
	_secondDate = [NSDate dateWithTimeIntervalSince1970:interval];
  }
  return _secondDate;
}

#pragma mark - Target Action

- (IBAction)calculateWithNSCalendar:(id)sender;
{
  NSCalendar *gregorianCalendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
  
  unsigned int unitFlags = NSHourCalendarUnit | NSMinuteCalendarUnit | NSDayCalendarUnit | NSMonthCalendarUnit;
  
  NSDateComponents *conversionInfo = [gregorianCalendar components:unitFlags
														  fromDate:self.firstDate
															toDate:self.secondDate
														   options:0];
  
  int months = [conversionInfo month];
  int days = [conversionInfo day];
  int hours = [conversionInfo hour];
  int minutes = [conversionInfo minute];
  
  self.outputLabel.text = [NSString stringWithFormat:@"%d months , %d days, %d hours, %d min", months, days, hours, minutes];
}

@end