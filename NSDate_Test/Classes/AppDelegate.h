//
//  AppDelegate.h
//  tet
//
//  Created by  Joe Rouleau on 1/8/15.
//  Copyright (c) 2015  Joe Rouleau. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
