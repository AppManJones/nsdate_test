//
//  NSDateFormatter+Predefined.h
//  CMS Client Library
//
//  Created by  Joe Rouleau on 06.05.14.
//  Copyright (c) 2014  Joe Rouleau. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDateFormatter (Predefined)

// Returns NSDateFormatter with date format:
// YYYY-MM-DD hh:mm:ss
+ (id)defaultDateFormatter;

+ (instancetype)mediumStyledDateFormatter;

@end
