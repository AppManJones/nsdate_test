//
//  NSDateFormatter+Predefined.m
//  CMS Client Library
//
//  Created by  Joe Rouleau on 06.05.14.
//  Copyright (c) 2014  Joe Rouleau. All rights reserved.
//

#import "NSDateFormatter+Predefined.h"

static NSDateFormatter *defaultDateFormatter = nil;
static NSDateFormatter *mediumStyledDateFormatter = nil;

@implementation NSDateFormatter (Predefined)

+ (instancetype)defaultDateFormatter;
{
  if ( !defaultDateFormatter ) {
	defaultDateFormatter = [[NSDateFormatter alloc] init];
	[defaultDateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
  }
  
  return defaultDateFormatter;
}

+ (instancetype)mediumStyledDateFormatter;
{
  if ( !mediumStyledDateFormatter ) {
	mediumStyledDateFormatter = [[self alloc] init];
	[mediumStyledDateFormatter setDateStyle:NSDateFormatterMediumStyle];
  }
  
  return mediumStyledDateFormatter;
}

@end
